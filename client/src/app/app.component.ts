import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  manuIsVisible=false;

  mInPage = 'home';
  constructor(){}
  toggleMenu(){
    this.manuIsVisible = !this.manuIsVisible;
  }
  togglePge(p){
    this.mInPage = p;
  }
}
