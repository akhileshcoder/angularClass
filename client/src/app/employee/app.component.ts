import { Component, AfterViewInit} from '@angular/core';
import { Http } from '@angular/http';

@Component({
  selector: 'app-employee',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class Employee implements AfterViewInit{
  employeeList = [];
  showModel = false;
  tempEmployee:any={};
  constructor(private http:Http){

  }
  ngAfterViewInit(){
    this.http.get('http://localhost:3200/employee-list').subscribe(data=>{
      this.employeeList = data.json();
    })
  }
  addEmployee(){
    let data = this.tempEmployee;
    console.log(data);

    this.http.post('http://localhost:3200/add-employee',{employee:data}).subscribe(data=>{
      this.employeeList = data.json();
      this.tempEmployee = {};
      this.showModel = false;
    })
  }
}
